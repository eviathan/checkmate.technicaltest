﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using Checkmate.FishTank.Interfaces;
using Checkmate.FishTank.Models;
using Microsoft.Extensions.Logging;

namespace Checkmate.FishTank.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            // NOTE: Normally these would be automatically DI'ed into the Tanks constructor
            var logger = new Logger();
            var serializer = new XMLSerializer(logger);
            var persistenceService = new FilePersistenceService(serializer);

            var tank = new Tank(logger, persistenceService)
            {
                Fish = new List<IFish>
                {
                    new AngelFish
                    {
                        Name = "Sharky"
                    },
                    new AngelFish
                    {
                        Name = "George"
                    },
                    new BabelFish
                    {
                        Name = "Speaky"
                    },
                    new GoldFish
                    {
                        Name = "Nemo"
                    }
                }
            };

            logger.LogInformation($"Feed amount = {tank.Feed()}g");

            tank.SaveFish("fish.xml");

            var newTank = new Tank(logger, persistenceService);
            newTank.LoadFish("fish.xml");

            logger.LogInformation($"New Tank Feed amount = {newTank.Feed()}g");
        }
    }
}
