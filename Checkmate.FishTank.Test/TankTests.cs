using System;
using System.Collections.Generic;
using Checkmate.FishTank.Interfaces;
using Checkmate.FishTank.Models;
using Xunit;
using FluentAssertions;

namespace Checkmate.FishTank.Test
{
    public class TankTests : TestBase
    {
        [Fact]
        public void IfThereAreFish_Feed_ShouldReturnTheSumOfFishFeedAmount()
        {
            // Arrange
            var tank = new Tank(_logger, _persistenceService)
            {
                Fish = new List<IFish>
                {
                    new AngelFish
                    {
                        Name = "Sharky",
                    },
                     new AngelFish
                    {
                        Name = "George",
                    },
                    new BabelFish
                    {
                        Name = "Flounder",
                    },
                    new GoldFish
                    {
                        Name = "Angel"
                    }
                }
            };

            // Act
            var amount = tank.Feed();

            // Assert
            amount.Should()
                  .Be(0.8f);
        }

        [Fact]
        public void IfThereAreNoFish_Feed_ShouldReturnZero()
        {
            // Arrange
            var tank = new Tank(_logger, _persistenceService);

            // Act
            var amount = tank.Feed();

            // Assert
            amount.Should()
                  .Be(0.0f);
        }
    }
}
