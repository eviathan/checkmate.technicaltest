﻿using System;
using Checkmate.FishTank.Interfaces;
using Microsoft.Extensions.Logging;

namespace Checkmate.FishTank.Test
{
    public abstract class TestBase
    {
        protected readonly ILogger _logger;
        protected readonly ISerializer _serializer;
        protected readonly IPersistenceService _persistenceService;

        public TestBase()
        {
            _logger = new Logger();
            _serializer = new XMLSerializer(_logger);
            _persistenceService = new FilePersistenceService(_serializer);
        }
    }
}
