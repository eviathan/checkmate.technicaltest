﻿using System;
namespace Checkmate.FishTank.Interfaces
{
    public interface ISerializer
    {
        string Serialize<T>(T type) where T : class;

        T DeSerialize<T>(string xmlString) where T : class;
    }
}
