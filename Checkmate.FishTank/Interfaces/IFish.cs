﻿using System;
using System.Xml.Serialization;

namespace Checkmate.FishTank.Interfaces
{
    public interface IFish
    {
        string Name { get; set; }

        float FoodAmount { get; }
    }
}
