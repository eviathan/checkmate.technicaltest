﻿using System;
namespace Checkmate.FishTank.Interfaces
{
    public interface IPersistenceService
    {
        void Save<T>(string path, T obj) where T : class;

        T Load<T>(string path) where T : class;
    }
}
