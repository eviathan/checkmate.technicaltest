﻿using System;
using System.Xml.Serialization;
using Checkmate.FishTank.Interfaces;

namespace Checkmate.FishTank.Models
{
    public class AngelFish : IFish
    {
        public string Name { get; set; }

        public float FoodAmount { get; } = 0.2f;
    }
}
