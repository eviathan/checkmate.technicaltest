﻿using System;
using Checkmate.FishTank.Interfaces;

namespace Checkmate.FishTank.Models
{
    public class GoldFish : IFish
    {
        public string Name { get; set; }

        public float FoodAmount { get; } = 0.1f;
    }
}
