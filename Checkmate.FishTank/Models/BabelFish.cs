﻿using System;
using Checkmate.FishTank.Interfaces;

namespace Checkmate.FishTank.Models
{
    public class BabelFish : IFish
    {
        public string Name { get; set; }

        public float FoodAmount { get; } = 0.3f;
    }
}
