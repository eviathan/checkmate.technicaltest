﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Checkmate.FishTank.Interfaces;
using Microsoft.Extensions.Logging;

namespace Checkmate.FishTank.Models
{
    public class Tank 
    {
        public List<IFish> Fish { get; set; } = new List<IFish>();

        private readonly ILogger _logger;
        private readonly IPersistenceService _persistenceService;

        public Tank(ILogger logger, IPersistenceService persistenceService)
        {
            _logger = logger;
            _persistenceService = persistenceService;
        }

        public float Feed()
        { 
            return Fish.Sum(fish => fish.FoodAmount);
        }

        public void SaveFish(string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                throw new ArgumentOutOfRangeException("Path must be a valid file path");

            try
            {
                _persistenceService.Save(path, Fish);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                throw new Exception("Could not save fish");
            }
        }

        public void LoadFish(string path)
        {
            if (string.IsNullOrWhiteSpace(path) || !File.Exists(path))
                throw new ArgumentOutOfRangeException("Path must be a valid file path");

            try
            {
                Fish = _persistenceService.Load<List<IFish>>(path);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                throw new Exception("Could not load fish");
            }
        }
    }
}
