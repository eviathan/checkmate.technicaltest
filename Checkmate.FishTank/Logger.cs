﻿using System;
using System.Collections;
using Microsoft.Extensions.Logging;

namespace Checkmate.FishTank
{
    public class Logger : ILogger
    {
        public IDisposable BeginScope<TState>(TState state) => default;

        public bool IsEnabled(LogLevel logLevel) => true;

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            switch (logLevel)
            {
                case LogLevel.Error:
                    Console.ForegroundColor = ConsoleColor.Red;                    
                    break;
                case LogLevel.Warning:
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    break;
                default:
                    Console.ForegroundColor = ConsoleColor.White;
                    break;
            }

            if(exception != null)
                Console.WriteLine($"{Enum.GetName(typeof(LogLevel), logLevel)}: {exception.Message}");

            if (state != null && state is IEnumerable states)
                Console.WriteLine(string.Join(Environment.NewLine, $"{Enum.GetName(typeof(LogLevel), logLevel)}: {states}"));
        }
    }
}
