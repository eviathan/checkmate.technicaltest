﻿using System;
using System.IO;
using Checkmate.FishTank.Interfaces;
using ExtendedXmlSerializer;
using ExtendedXmlSerializer.Configuration;
using Microsoft.Extensions.Logging;

namespace Checkmate.FishTank
{
    public class XMLSerializer : ISerializer
    {
        private readonly ILogger _logger;

        public XMLSerializer(ILogger logger)
        {
            _logger = logger;
        }

        public string Serialize<T>(T obj) where T : class
        {
            try
            {
                var serializer = new ConfigurationContainer().Create();
                return serializer.Serialize(obj);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                throw new Exception("Failed to serialise object");
            }
        }

        public T DeSerialize<T>(string xmlString) where T : class
        {
            try
            {
                var serializer = new ConfigurationContainer().Create();
                return serializer.Deserialize<T>(xmlString);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, ex);
                throw new Exception("Failed to deserialise file");
            }
        }
    }
}
