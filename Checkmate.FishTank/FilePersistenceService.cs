﻿using System;
using System.Collections.Generic;
using System.IO;
using Checkmate.FishTank.Interfaces;

namespace Checkmate.FishTank
{
    public class FilePersistenceService : IPersistenceService
    {
        private readonly ISerializer _serializer;

        public FilePersistenceService(ISerializer serializer)
        {
            _serializer = serializer;
        }

        public void Save<T>(string path, T obj) where T : class
        {
            var xmlString = _serializer.Serialize(obj);

            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.Write(xmlString);
            }
        }

        public T Load<T>(string path) where T : class
        {
            using (StreamReader sr = new StreamReader(path))
            {
                var xmlString = sr.ReadToEnd();
                return _serializer.DeSerialize<T>(xmlString);
            }
        }
    }
}
