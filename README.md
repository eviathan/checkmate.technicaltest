# Checkmate - Technical Test 2020

## Brief
I need a new C# class library (any Unity/.net version) to help me manage my fish tank. Don’t worry about the UI: I will build that. The library should satisfy the following user stories and demonstrate your design, coding and testing abilities.

## User stories

1. A user should be able to add 3 types of fish to the tank (Gold fish, Angel fish, Babel fish) and name the fish
2. A user should be able to see how much food to put in the tank with a Tank.Feed()  method.
    - This should return the weight in grams of the total required fish food. 
        - 0.1g for each Gold  fish
        - 0.2g for each Angel fish
        - 0.3g for each Babel fish
3. A user should be able to save the data about the fish in the tank to an xml file
4. Ensure the design allows me to add more types of fish in the future without having to change the tank class.
